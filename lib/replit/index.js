/*jshint globalstrict:true, trailing:false, unused:true, node:true */
"use strict";

function parseReplitWebhook(headers, body, settings) {

    var assignmentName = body.assignment.name;
    var studentName = body.student.first_name + " " +
        body.student.last_name;

    var submissionUrl = body.submission.teacher_url;
    var submissionStatus = body.submission.status;

    var info = studentName + " submitted **" + submissionStatus +
        "** for " + assignmentName;
    var md_url_info = "Click [here]" + "(" + submissionUrl + ") to review this submission.";

    var message = info + "  \n" + md_url_info;

    return {
      message: message,
      icon: 'logo',
      errorLevel: 'normal'
    };
}

module.exports = {
  name: 'replit',
  apiVersion: 1,
  parse: parseReplitWebhook
};
